(function () {
    module.exports = function(app){
        const Ctrl = require('./produits.controller')();
        app.route('/produits')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/produits/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)
    }
})();