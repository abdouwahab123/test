(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var produitSchema = new Schema({
        nom: { type : String, required : true},
        taille: { type : String, required : true},
        dateExp: { type : String, required : false}
    });

    module.exports = {
        produitModel : mongoose.model('Produits', produitSchema)
    }
})();
