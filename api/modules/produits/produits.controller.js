(function () {
    const Produits= require('./produits.model').produitModel;
    module.exports = function(){
        return {
            list: function(req,res){
                Produits.find({},(error,list) =>{
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                });
            },
            create: function(req,res) {
                console.log('save',req.body);
                var produit = new Produits(req.body);
                produit.save((error,rep)=>{
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
                
            },
            read: function(req,res){
                Produits.findOne({_id: req.params.id}, function(error, produit){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!produit){
                        res.status(404).json({
                            status: "not found",
                            body: produit
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: produit
                        })
                    }
                })
            },
            update: function(req,res){
                Produits.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, produit){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: produit
                        })
                    }
                })
            },
            delete: function (req,res) {
                Produits.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }
})();